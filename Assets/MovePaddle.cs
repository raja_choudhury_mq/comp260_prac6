﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour
{
	private Rigidbody rigidbody;

	public bool useKeyboard;

	public float speed = 20f;

	public string horizontalAxis;
	public string verticalAxis;

	void Start()
	{
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	void Update()
	{
		Debug.Log("Time = " + Time.time);
	}

	void FixedUpdate()
	{
		if (useKeyboard)
		{
			Vector3 pos = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
			rigidbody.AddForce(speed * pos);
		}
		else
		{
			Vector3 pos = GetMousePosition();
			Vector3 dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			// check is this speed is going to overshoot the target
			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;

			if (move > distToTarget)
			{
				// scale the velocity down appropriately
				vel = vel * distToTarget / move;
			}

			rigidbody.velocity = vel;
		}
	}

	private Vector3 GetMousePosition()
	{
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos()
	{
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}
}
